#include <iomanip>

#include "Game.h"
#include "WindowUtils.h"
#include "CommonStates.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

MouseAndKeys Game::sMKIn;
Gamepads Game::sGamepads;


Game::Game(MyD3D& d3d)
	: mPMode(d3d), mD3D(d3d)
{
	sMKIn.Initialise(WinUtil::Get().GetMainWnd(), true, false);
	sGamepads.Initialise();
	mpSB = new SpriteBatch(&mD3D.GetDeviceCtx());
	assert(mpSB);
	mpF = new SpriteFont(&mD3D.GetDevice(), L"data/fonts/comicSansMS.spritefont");
	assert(mpF);
}


//any memory or resources we made need releasing at the end
void Game::Release()
{
	delete mpSB;
	mpSB = nullptr;
	delete mpF;
	mpF = nullptr;
}

//called over and over, use it to update game logic
void Game::Update(float dTime)
{
	sGamepads.Update();
	switch (state)
	{
	case State::PLAY:
		mPMode.Update(dTime);
	}
}

//called over and over, use it to render things
void Game::Render(float dTime)
{
	mD3D.BeginRender(Colours::Black);


	CommonStates dxstate(&mD3D.GetDevice());
	mpSB->Begin(SpriteSortMode_Deferred, dxstate.NonPremultiplied(), &mD3D.GetWrapSampler());

	switch (state)
	{
	case State::PLAY:
		mPMode.Render(dTime, *mpSB, *mpF);
	}

	mpSB->End();


	mD3D.EndRender();
	sMKIn.PostProcess();
}

//*********************************************************************************
void Asteroid::Init()
{
	vector<RECTF> frames2;
	frames2.insert(frames2.begin(), 8 * 8, RECTF());
	const float ASS_SZ = 102;
	int c = 0;
	for (int y = 0; y < 8; ++y)
		for (int x = 0; x < 8; ++x)
			frames2[c++] = RECTF{ x*ASS_SZ,y*ASS_SZ,x*ASS_SZ + ASS_SZ,y*ASS_SZ + ASS_SZ };
	ID3D11ShaderResourceView* p = spr.GetD3D().GetCache().LoadTexture(&spr.GetD3D().GetDevice(), "asteroid.dds", "asteroid", true, &frames2);

	spr.SetTex(*p);
	spr.GetAnim().Init(0, 31, 15, true);
	spr.GetAnim().Play(true);
	spr.SetScale(Vector2(0.5f, 0.5f));
	spr.origin = Vector2(ASS_SZ / 2.f, ASS_SZ / 2.f);
	active = false;
}

void Asteroid::Render(SpriteBatch& batch)
{
	if (active)
		spr.Draw(batch);
}

void Asteroid::Update(float dTime)
{
	if (active)
	{
		float radius = spr.GetScreenSize().Length() / 2.f;
		spr.mPos.x -= GC::ASTEROID_SPEED * dTime;
		if (spr.mPos.x < -radius)
			active = false;
		spr.GetAnim().Update(dTime);
	}
}

//******************************************************************************
PlayMode::PlayMode(MyD3D & d3d)
	:mD3D(d3d)
{
	InitBgnd();
	InitRoids();
}



void PlayMode::Update(float dTime)
{
	UpdateBgnd(dTime);
	UpdateRoids(dTime);
	HandleInput(dTime);
}

void PlayMode::HandleInput(float dTime)
{
	if (Game::sMKIn.IsPressed(VK_LEFT))
	{
		mSpawnRateSec -= GC::ASTEROID_SPAWN_INC * dTime;
		if (mSpawnRateSec < 0)
			mSpawnRateSec = 0;
	}
	else if (Game::sMKIn.IsPressed(VK_RIGHT))
	{
		mSpawnRateSec += GC::ASTEROID_SPAWN_INC * dTime;
		if (mSpawnRateSec > GC::ASTEROID_MAX_SPAWN_DELAY)
			mSpawnRateSec = GC::ASTEROID_MAX_SPAWN_DELAY;
	}
}

void PlayMode::Render(float dTime, SpriteBatch & batch, SpriteFont& font) 
{
	RenderBgnd(batch);
	RenderRoids(batch);
	UpdateText(batch,font);
}

void PlayMode::UpdateText(SpriteBatch& batch, SpriteFont& font)
{
	wstringstream ss;
	ss.precision(3);
	ss << mSpawnRateSec;
	wstring msg = L"Spawn delay(secs): ";
	msg += ss.str();
	RECT r = font.MeasureDrawBounds(msg.c_str(), Vector2(0, 0));
	Vector2 pos{ 10, WinUtil::Get().GetClientHeight() - r.bottom*1.1f };
	font.DrawString(&batch, msg.c_str(), pos);
}

Asteroid *PlayMode::CheckCollRoids(Asteroid& me)
{
	assert(!mAsteroids.empty());
	float radius = mAsteroids[0].spr.GetScreenSize().Length() / 2.f;
	size_t i = 0;
	Asteroid *pColl = nullptr;
	while (i < mAsteroids.size() && !pColl)
	{
		Asteroid& collider = mAsteroids[i];
		if ((&me!=&collider) && 
			collider.active && 
			(collider.spr.mPos - me.spr.mPos).Length() < (radius * 2))
			pColl=&mAsteroids[i];
		++i;
	}
	return pColl;
}

Asteroid* PlayMode::SpawnRoid()
{
	assert(!mAsteroids.empty());
	size_t i = 0;
	Asteroid*p = nullptr;
	while (i < mAsteroids.size() && !p)
	{
		if (!mAsteroids[i].active)
			p = &mAsteroids[i];
		++i;
	}

	if (p)
	{
		int w, h;
		WinUtil::Get().GetClientExtents(w, h);
		float radius = mAsteroids[0].spr.GetScreenSize().Length() / 2.f;
		Vector2& pos = p->spr.mPos;
		pos.y = (float)GetRandom(radius, h - radius);
		pos.x = (float)(w + radius);
		bool collision = false;
		if (CheckCollRoids(*p))
			collision = true;
		if (!collision)
			p->active = true;
		else
			p = nullptr;
	}
	return p;
}

void PlayMode::UpdateRoids(float dTime)
{
	assert(!mAsteroids.empty());
	for (auto& a : mAsteroids)
		a.Update(dTime);

	if ((GetClock() - mLastSpawn) > mSpawnRateSec)
	{
		if(SpawnRoid())
			mLastSpawn = GetClock();
	}
}

void PlayMode::InitRoids()
{
	assert(mAsteroids.empty());
	Asteroid a(mD3D);
	a.Init();
	mAsteroids.insert(mAsteroids.begin(), GC::ROID_CACHE, a);
	for (auto& a : mAsteroids)
	{
		if (GetRandom(0, 1) == 0)
			a.spr.GetAnim().Init(0, 31, GetRandom(10.f, 20.f), true);
		else
			a.spr.GetAnim().Init(32, 63, GetRandom(10.f, 20.f), true);
		a.spr.GetAnim().SetFrame(GetRandom(a.spr.GetAnim().GetStart(), a.spr.GetAnim().GetEnd()));
	}
}

void PlayMode::RenderRoids(SpriteBatch & batch)
{
	for (auto& a : mAsteroids)
		a.Render(batch);
}


//*****************************************************************************
void PlayMode::UpdateBgnd(float dTime)
{
	//scroll the background layers
	int i = 0;
	for (auto& s : mBgnd)
		s.Scroll(dTime*(i++)*GC::SCROLL_SPEED, 0);
}

void PlayMode::InitBgnd()
{
	//a sprite for each layer
	assert(mBgnd.empty());
	mBgnd.insert(mBgnd.begin(), GC::BGND_LAYERS, Sprite(mD3D));

	//a neat way to package pairs of things (nicknames and filenames)
	pair<string, string> files[GC::BGND_LAYERS]{
		{ "bgnd0","backgroundlayers/mountains01_007.dds" },
		{ "bgnd1","backgroundlayers/mountains01_005.dds" },
		{ "bgnd2","backgroundlayers/mountains01_004.dds" },
		{ "bgnd3","backgroundlayers/mountains01_003.dds" },
		{ "bgnd4","backgroundlayers/mountains01_002.dds" },
		{ "bgnd5","backgroundlayers/mountains01_001.dds" },
		{ "bgnd6","backgroundlayers/mountains01_000.dds" },
		{ "bgnd7","backgroundlayers/mountains01_006.dds" }
	};
	int i = 0;
	for (auto& f : files)
	{
		//set each texture layer
		ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), f.second, f.first);
		if (!p)
			assert(false);
		mBgnd[i++].SetTex(*p);
	}

}

void PlayMode::RenderBgnd(SpriteBatch & batch)
{
	for (auto& s : mBgnd)
		s.Draw(batch);
}
