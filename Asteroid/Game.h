#pragma once

#include <vector>

#include "Input.h"
#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "SpriteFont.h"

namespace GC
{
	const float SCROLL_SPEED = 10.f;
	const int BGND_LAYERS = 8;
	const float MOUSE_SPEED = 5000;
	const float PAD_SPEED = 500;
	const float ASTEROID_SPEED = 100;
	const float ASTEROID_SPAWN_RATE = 1;
	const float ASTEROID_SPAWN_INC = 1;
	const float ASTEROID_MAX_SPAWN_DELAY = 10;
	const int ASTEROID_SPAWN_TRIES = 50;
	const int ROID_CACHE = 32;
}


/*
Animated asteroid 
*/
struct Asteroid
{
	Asteroid(MyD3D& d3d)
		:spr(d3d)
	{}
	Sprite spr;
	bool active = false;	//should it render and animate?

	//setup
	void Init();
	/*
	draw - the atlas has two asteroids, half frames in one, half the other
	asteroids are randomly assigned one and then animate and at random fps
	*/
	void Render(DirectX::SpriteBatch& batch);
	/*
	move left until offscreen, then go inactive
	*/
	void Update(float dTime);
};


//horizontal scrolling with player controlled ship
class PlayMode
{
public:
	PlayMode(MyD3D& d3d);
	void Update(float dTime);
	void Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font);

private:

	MyD3D& mD3D;
	std::vector<Sprite> mBgnd; //parallax layers
	RECTF mPlayArea;	//don't go outside this	
	std::vector<Asteroid> mAsteroids;	//a cache of asteroids
	float mSpawnRateSec= GC::ASTEROID_SPAWN_RATE; //how fast to spawn in new asteroids
	float mLastSpawn = 0;	//used in spawn timing
	
	//scroll the background parallax layers
	void UpdateBgnd(float dTime);
	void InitBgnd();
	void RenderBgnd(DirectX::SpriteBatch & batch);

	//scroll in asteroids randomly
	/*
	Try to make one just off screen to the right that definitely 
	isn't touching anything else.
	*/
	Asteroid* SpawnRoid();
	/*
	Give each active roid an update and monitor the spawn delay
	creating new roids as needed
	*/
	void UpdateRoids(float dTime);
	//create a cache of roids to use and re-use
	void InitRoids();
	//ask each roid if it wants to render
	void RenderRoids(DirectX::SpriteBatch & batch);
	//me - an asteroid to check for collision with all other active ones
	Asteroid *CheckCollRoids(Asteroid& me);
	//vary the spawn rate
	void HandleInput(float dTime);
	//display spawn rate
	void UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font);
};


/*
Basic wrapper for a game
*/
class Game
{
public:
	enum class State { PLAY };
	static MouseAndKeys sMKIn;
	static Gamepads sGamepads;
	State state = State::PLAY;
	Game(MyD3D& d3d);


	void Release();
	void Update(float dTime);
	void Render(float dTime);
private:
	MyD3D& mD3D;
	DirectX::SpriteBatch *mpSB = nullptr;
	DirectX::SpriteFont *mpF = nullptr;
	//not much of a game, but this is it
	PlayMode mPMode;
};


